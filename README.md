# Artifact Repository Manager Nexus



## Project Description
One of the most popular artifact repository manager is Nexus. The main purpose is to stored different versions of the build artfact, that 
can latter be push to a deployment environment or pull into the local development environment for adjusting bug fixing if the recent build artifact enounter
error. It can have multiple versions of the artifcts for a single application. 
Additionally, it manages different types of artfacts like the ones from java application, nodje apllication and docker images.
Inorder to avoid the storage capacity of the Nexus repository being full with
artfacts, one can configure clean-up policies where by older artfacts can be deleted after a certain conditions- eg clean up rule can be set to delete aftifacts that are older than 30 days for example. 

The project is about creatig a nexus repository from scratch, configurng it on a Linux remote server on DigitalOcean and pushing difeerent version of the artfacts to it.



The workflow involves:
- Create a remote server, installed and configure Nexus on it
- Build an artfact for a Java application - jar using the build tool gradle and push/upload it   to the Nexus Repository
- Create a new Nexus user and provide the user with the relevant previledge and permission.
- In company setting, the existing user at the company ar inegrated with Nexus using LDAP
- Use the created Nexus user to upload/push artifacts to Nexus hosted repository from local environment(laptop or Desktop).
To do the last point, we will need to add a plugin for publishing to nexus in our build.gradle file locally. The plushing section will contain the build artifact and a repository on which the artifact is to be publish/upload. A gradle property file will be created, to store the created Nexus user credential, and called using environmental variables. These file will not be comitted into remote git repo like GitLab since it contain sensitive information. .gitignore is used to achieve the process of nopt commiting specific files.



Skills Aquired:

Platform: Linux OS

Cloud Provider: DigitalOcean

Build Tools: Apache Maven, Gradle npm

Programming Language: Java

Repository Manager: Nexus


```
cd existing_repo
git remote add origin https://gitlab.com/techagombecho/artifact-repository-manager-nexus.git
git branch -M main
git push -uf origin main
```

